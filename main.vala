using Draft;
using Gtk;

class Draft.Main : GLib.Object
{
    Window window;
    Builder builder;
    Draft.Configuration config;

    public Main(Builder builder) {
        this.builder = builder;
        this.window = builder.get_object ("window") as Window;
        create_toolbar();
        setup_accelerators();
        window.show_all();
    }

    private void get_focus_widget(Gtk.Action action, Widget widget)
    {
        widget.grab_focus();
    }

    private void setup_sourceview()
    {

    }

    private void create_toolbar()
    {
        var box = this.builder.get_object ("box_button1") as Box;
        var action_new = this.builder.get_object ("action_new") as Gtk.Action;
        var action_open = this.builder.get_object ("action_open") as Gtk.Action;
        var action_save = this.builder.get_object ("action_save") as Gtk.Action;
        var action_search_focus = this.builder.get_object ("action_search_focus") as Gtk.Action;
        box.pack_start(action_new.create_tool_item(), false, false, 0);
        box.pack_start(action_open.create_tool_item(), false, false, 0);
        box.pack_start(action_save.create_tool_item(), false, false, 0);
        box.pack_start(action_search_focus.create_tool_item(), false, false, 0);
    }

    private void setup_accelerators()
    {
        var ui_manager = this.builder.get_object ("uiman") as UIManager;
        
        this.window.add_accel_group(ui_manager.get_accel_group());

    }
}

public static int main(string[] args)
{
    try {
        Gtk.init (ref args);
        var builder = new Builder ();
        builder.add_from_file ("draft.glade");
        builder.connect_signals (null);
        var draft = new Draft.Main(builder);
        Gtk.main();
    } catch (Error e) {
        stderr.printf ("Could not load UI: %s\n", e.message);
        return 1;
    }
    return 0;
}
