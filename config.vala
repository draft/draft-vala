using GConf;

/*
 * This class manages the application settings.
 * It loads and saves preferences using gconf.
 */
class Draft.Configuration : GLib.Object {
    public bool line_num = true;
    public bool wrap_word = false;
    public bool show_right_margin = false;
    public int right_margin = 80;
    private Engine engine;
    private string GCONF_NAME = "/apps/draft/";
    
    public Configuration () {
        engine = Engine.get_default();
    }
    
    /* Saves the settings in gconf */
    public void save_settings() {
        try {
            engine.set_bool(GCONF_NAME+"line_num", this.line_num);
            engine.set_bool(GCONF_NAME+"wrap_word", this.wrap_word);
            engine.set_bool(GCONF_NAME+"show_right_margin", this.show_right_margin);
            engine.set_int(GCONF_NAME+"right_margin", this.right_margin);
        } catch (GLib.Error e) {
            stderr.printf("Error: %s\n", e.message);
        }
    }
    
    /* Loads the settings from gconf */
    public void load_settings() {
        try {
            this.line_num = engine.get_bool(GCONF_NAME+"line_num");
            this.wrap_word = engine.get_bool(GCONF_NAME+"wrap_word");
            this.show_right_margin = engine.get_bool(GCONF_NAME+"show_right_margin");
            this.right_margin = engine.get_int(GCONF_NAME+"right_margin");
        } catch (GLib.Error e) {
            stderr.printf("Error: %s\n", e.message);
        }
    }
    
    /* Save given key with given value */
    public void save_key(string key, GConf.Value value) {
        try {
            engine.set(GCONF_NAME+key, value);
        } catch (GLib.Error e) {
            stderr.printf("Error: %s\n", e.message);
        }
    }
    
    public void apply_settings() {
    
    }
}